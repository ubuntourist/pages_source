.. include:: /_static/isonum.txt

.. Linux Tips master file, created by
   sphinx-quickstart on Sat Sep  5 17:13:07 2020.
   You can adapt this file completely to your liking, but it
   should at least contain the root `toctree` directive.

Linux Tips
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   LinuxTips
