# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath("."))


# -- Project information -----------------------------------------------------

project = "The Ubuntourist Trap"
copyright = """2020, Kevin Cole ("The Accidental Ubuntourist")"""
author = """Kevin Cole ("The Accidental Ubuntourist")"""

# The short X.Y version
version = ""
# The full version, including alpha/beta/rc tags
release = "0.1"


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = "1.0"

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named "sphinx.ext.*") or your custom
# ones.
extensions = [
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
#   "sphinx.ext.mathjax",
    "sphinx.ext.imgmath",
    "sphinx.ext.ifconfig",
    "sphinx.ext.viewcode",
    "cloud_sptheme.ext.table_styling",
    "sphinx_simplepdf",
]

# Turn on Simple PDF debugging
#
simplepdf_debug = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = [".rst", ".md"]
source_suffix = ".rst"

# The master toctree document.
master_doc = "index"

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = "en"

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = None


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = "alabaster"
# html_theme = "default"
# html_theme = "agogo"
# html_theme = "basic"
# html_theme = "bizstyle"
# html_theme = "classic"
# html_theme = "epub"
# html_theme = "haiku"
# html_theme = "nature"
# html_theme = "nonav"
# html_theme = "pyramid"
# html_theme = "scrolls"
# html_theme = "traditional"
# html_theme = "sphinxdoc"
html_theme   = "cloud"
# html_theme = "custom"   # See other KJC projects

# 2022.12.28 KJC - Change "|" to "%7C" in Google API fonts URL
html_theme_options = {"fontcssurl": "https://fonts.googleapis.com/css?family=Noticia+Text:400,i,b,bi%7COpen+Sans:400,i,b,bi%7CRoboto+Mono:400,i,b,bi&display=swap"}

# 2022.08.31 KJC - Add a favorite icon
#html_favicon = "_static/icon-lightning.svg"

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {"nosidebar": "true"}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: """["localtoc.html", "relations.html", "sourcelink.html",
#              "searchbox.html"]""".
#
# html_sidebars = {}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = "UbuntouristTrapdoc"


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ("letterpaper" or "a4paper").
    #
    # "papersize": "letterpaper",

    # The font size ("10pt", "11pt" or "12pt").
    #
    # "pointsize": "10pt",

    # Additional stuff for the LaTeX preamble.
    #
    # "preamble": "",

    # Latex figure (float) alignment
    #
    # "figure_align": "htbp",
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [(master_doc,
                    "UbuntouristTrap.tex",
                    "The Ubuntourist Trap",
                    author,
                    "manual"),]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [(master_doc,
              "ubuntouristtrap",
              "The Ubuntourist Trap",
              [author],
              1),]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [(master_doc,
                      "UbuntouristTrap",
                      "The Ubuntourist Trap",
                      author,
                      "UbuntouristTrap",
                      "A collection of technical writings, ancient and new.",
                      "Miscellaneous"),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ""

# A unique identification for the text.
#
# epub_uid = ""

# A list of files that should not be packed into the epub file.
epub_exclude_files = ["search.html"]


# -- Extension configuration -------------------------------------------------

# -- Options for imgmath extension -------------------------------------------
#
imgmath_image_format = "svg"  # or "png". Requires dvisvgm or dvipng
imgmath_use_preview = True    # or False. Requires preview-latex-style
imgmath_add_tooltips = True   # See if there's alt text
imgmath_font_size = 14        # Default = 12
imgmath_latex = "latex"       # Default. Later maybe "xelatex" or "latexmk"
imgmath_embed = True          # Bug work-around. Do NOT make separate SVG.
imgmath_latex_preamble = "\\usepackage{newtxsf}"

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
#
# 2023.11.09 KJC - Fixed as described at:
# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#confval-intersphinx_mapping
#
intersphinx_mapping = { "python": ("https://docs.python.org/3", None) }

# -- Options for todo extension ----------------------------------------------

# If true, "todo" and "todoList" produce output, else they produce nothing.
todo_include_todos = True

rinoh_documents = [dict(doc="index",            # top-level file (index.rst)
                        target="publication")]  # output file (publication.pdf)
