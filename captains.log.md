# Captain's Log - Integrating Sphinx and Codeberg

## 2020.08.30 (KJC)

Edzon discovered that Codeberg supports static web sites in a fashion
similar to GitHub. If Codeberg finds a repository named **pages** it
will render the HTML at https://pages.codeberg.org/username which
in my case will be https://pages.codeberg.org/ubuntourist

So now I have initialized **pages** and placed the repository in
`~/gits/websites/pages.codeberg.org/`.

Meanwhile, here, in this directory, I've created a **Sphinx** project
with `sphinx-quickstart`.  I've turned on **ALL** of the standard Sphinx
extensions. I may regret that. I've a good mind to find out if there
are other extensions that are non-standard, that will do what I am
about to do. To whit:

I want to alter the `Makefile` so that it can copy the generated HTML
to the git repository, and then add, commit and push to Codeberg. I
can either add a new `make` argument like "`publish`" -- probably the
better choice, or change the behavior of `make html`. The longer I
ponder, the more I like `make publish`.

----

## 2020.09.01 (KJC)

Attempting my first **hook** `post-commit`, which, with any luck,
will "publish" the site.

**Attempt 1**: Almost. It created a subdirectory `./html/`. I don't
want one. It should put the generated HTML at the root of the site
directory.

**Attempt 2**: Success!

----

## 2020.09.02 (KJC)

I was searching for a way to make better tables, and stumbled upon the
[Cloud theme for Sphinx](https://cloud-sptheme.readthedocs.io/) which
appears to be just the ticket!

----

## 2020.09.03 (KJC)

I began converting "**PDP-11 Assembler Made Painless (I Hope)**" with
a lot of work being focused on the tables scattered throughout. 

I'm a bit disappointed that there is no border at the bottom of the
table. (There's none at the top either, but it's less noticable, given
the table header has a different background color than the main text.)

But... I can see this is becoming a jumble. Since there's only one
"root" Sphinx directory with only a single `index.rst` / master table
of contents, multiple separate documents becomes awkward.

So. I'm going to need to move individual documents down a level, and
have a tree of `source` / `build` directory pairs, each with their own
Sphinx `Makefile` and `source/conf.py` configuration... Not to mention
a considerably more involved `.git/hooks/post-commit` script and a
directory tree in the `sites/` directory. 

Oh, and a main `Makefile` to run all the subroutine `Makefile`s.

----

## 2020.09.04 (KJC)

What a fucking nightmare! Such a simple concept: I want a generic
`conf.py` at a top level, and then several subdirectories, one
document each, that will import the parent `conf.py` and override a
few variables as subclassing overrides superclasses. You would think
`from .. import conf` in the subdirectories makes sense. But no. I
looked at example after example after example. I copied examples.  And
none worked. The solution that seemed to be very out of favor
worked. So screw 'em.

So in each subdirectory:

```
    >>> import sys
    >>> sys.path.append("..")
    >>> from conf import *
    >>> ...
    >>> copyright = "new copyright"
```

Not quite... Since both the main file and the file we're overriding are
named "`conf.py`" the `import` statement would probably be infinitely
recursive, searching for the **closest** `conf.py` -- itself -- rather
than the one that's up two directories and down another.

So... I'm not thrilled with it, but a **symbolic link** seems to solve
the issue...

```
    $ cd tips/source
    $ ln -s ../../conf.py parent.py
    $ cat > conf.py
    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-
    
    from parent import *

    # Override main document settings

    project           = "New Title"
    copyright         = "New copyright"
    author            = "New Author"
    latex_documents   = [(master_doc, 
                          'NewTitle.tex',
                          'New Title Documentation',
                          'New Author',
                          'manual'),]
     man_pages        = [(master_doc, 
                          'newtitle',
                          'New Title Documentation',
                          [author],
                          1)]
    texinfo_documents = [(master_doc,
                          'NewTitle',
                          'New Title Documentation',
                          author,
                          'NewTitle',
                          'One line description of project.',
                          'Miscellaneous'),]
    ^D

    $ python3
    >>> from parent import *
    >>> project
    'Ubuntourist Guide'
    >>> from conf import *
    >>> project
    'Linux Tips'
    >>> 

```

----

## 2020.09.05 (KJC)

Yesterday's borking of everything just made a bigger mess. Thankfully,
I didn't commit. It seems the correct way to do things is hang everything
under `./source/` and create `index.rst` files within each subdirectory.

----

## 2020.09.09

Adding a `rst_prolog` and/or `rst_epilog` to `conf.py` will
automatically include the contents at the start and/or end of every
`.rst` file. See [the
documentation](https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-rst_epilog)
For example:

```
    rst_prolog = """.. |psf| replace:: Python Software Foundation"""
```

However, it's still not really what I want: I want an environment
variable that points to the root `source/_static/` directory, not a
non-existent `_static/` directory relative to the current document's
directory. I may be able to beat the above into submission, but it
still feels hackish and wrong.

And... (hours later) ...it was wrong. Glad I didn't even bother trying
it. The solution was **MUCH** simpler:

```
    .. include:: /_static/isonum.txt
```

with an inital `/` rather than:

```
    .. include:: _static/isonum.txt
```

without it.

----

# 2022.11.22 (KJC)

* Attempting to defeat **Content Security Policy (CSP) silly rules.  I
  need to override bits of
  `~/.local/lib/python3.10/site-packages/cloud_sptheme/themes/cloud/layout.html`
  which overrides bits of
  `~/.local/lib/python3.10/site-packages/sphinx/themes/basic/layout.html`

----
