# Pages

A random assortment of what passed for tutorials, tips, and
documentation once upon a time. And maybe more. See:

https://pages.codeberg.org/ubuntourist

## Requirements

These pages were last compiled with:

* [`Python         3.10.4`](https://www.python.org/)
* [`Sphinx         5.1.1`](https://www.sphinx-doc.org/en/master/)
* [`cloud-sptheme  1.10.1.post20200504175005`](https://cloud-sptheme.readthedocs.io/en/latest/)

----

